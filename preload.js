/**
 * MultiSearch - Search using multiple search engines.
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const puppeteer = require('puppeteer')
const mustache = require('mustache')
const SearchEngine = require('./search-engine')
const MultiSearch = require('./multi-search')

// Configure the browsers that are to be used during multi search. 
const google = new SearchEngine(
  'Google', 
  'https://www.google.com/?gl=us&hl=en',
  '[title=Search]',
  '[id=search] > div > [data-async-context]',
  '.g',
  {
    title: 'h3',
    link: 'a',
    details: '.st'
  }
)

const duckduckgo = new SearchEngine(
  'DuckDuckGo',
  'https://duckduckgo.com/',
  '[id=search_form_input_homepage]',
  '.results',
  '.result__body', 
  { 
    title: '.result__a',
    link: '.result__a',
    details: '.result__snippet'
  }
)

let browser = null
let multiSearch = null

window.addEventListener('load', (event) => {
  (async() => {
     browser = await puppeteer.launch()
     multiSearch = new MultiSearch([duckduckgo, google], browser)
  })()
})

// Add an event listener to perfom search by user initiation.
window.addEventListener('message', event => {
  const message = event.data

  if (message.internalMessageType === 'perform-multi-search') {
    (async() => {
      const mergedResults = await multiSearch.search(message.searchString)
      console.log(mergedResults);
      
      const template = document.getElementById('articles-template').innerHTML
      const rendered = mustache.render(template, { articles: mergedResults })
      
      document.getElementById('search-results').innerHTML = rendered
    })()
  }
})