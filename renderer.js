/**
 * MultiSearch - Search using multiple search engines.
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const searchButton = document.getElementById('search-button')
const searchInput = document.getElementById('search-input') 

const performSearch = function() {
  console.log(searchInput.value)
  
  window.postMessage({
    internalMessageType: 'perform-multi-search',
    searchString: searchInput.value
  })
}

searchInput.onkeydown = function(current) {
  if(current.keyCode == 13) // Perform search when pressing enter in searchbar.
    performSearch()
}

searchButton.onclick = performSearch