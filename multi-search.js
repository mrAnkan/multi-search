/**
 * MultiSearch - Search using multiple search engines.
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

module.exports = class MultiSearch {

  /**
   * Instantiate MultiSearch.
   * @param {Array.<SearchEngine>} searchEngines List of search engines to use for searching.
   * @param {Browser} browser An instance of a puppeteer browser.
   */
  constructor(searchEngines, browser) {
    this.searchEngines = this._mapEngines(searchEngines)
    this.browser = browser
  }

  /**
   * Map a list of search engines to an object with search engine name as key and object as value.
   * @param {Array.<SearchEngine>} searchEngines List of engines to use for searching.
   */
  _mapEngines(searchEngines) {
    const engines = {}
    
    for(let i = 0; i < searchEngines.length; i++) {
      const engine = searchEngines[i]
      engines[engine.name] = engine
    }
    
    return engines
  }

  /**
   * Search using multiple search engines.
   * @param {String} searchString The string to use when searching.
   */
  async search(searchString) {
    const mergedResults = []
    const articlesByLink = {}
    for (const engineName in this.searchEngines) {
      if (this.searchEngines.hasOwnProperty(engineName)) {
        const engine = this.searchEngines[engineName]

        const raw = await engine.search(searchString, this.browser)

        await engine.merge(raw, mergedResults, articlesByLink)
      }
    }
      
    return mergedResults
  }
}