/**
 * MultiSearch - Search using multiple search engines.
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

module.exports = class SearchEngine {
  
  /**
   * Instantiate a SearchEngine.
   * @param {string} name The name of current search engine.
   * @param {*} uri The uri to the front search page of the search engine.
   * @param {*} inputField The path or identity of the search field.
   * @param {*} resultWrapper The wrapper for the result set/ list of articles.
   * @param {*} articleWrapper The wrapper for each article.
   * @param {*} filterMap The filter for extracting parts of each article.
   */
  constructor(name, uri, inputField, resultWrapper, articleWrapper, filterMap) {
    this.name = name
    this.uri = uri
    this.inputField = inputField
    this.resultWrapper = resultWrapper
    this.articleWrapper = articleWrapper
    this.filterMap = filterMap
  }
  
  /**
   * Search using current search engine in the provided browser.
   * @param {string} searchTerm A string to search for.
   * @param {Browser} browser A puppeteer browser.
   */
  async search(searchTerm, browser) {
    const page = await browser.newPage()
    
    await page.goto(this.uri)
  
    await this._debugImage('load', page, false)
  
    const inputField = await page.$(this.inputField)
    await inputField.type(searchTerm, { delay: 100 })
  
    await this._debugImage('search', page, false)
  
    await page.$eval("form", form => form.submit())
    await page.waitForNavigation({ waitUntil: ["load"] })
  
    await this._debugImage('firstpage', page, true)
  
    return page.$(this.resultWrapper)
  }

  /**
   * Filter and merge the results of articles to only present relevant data.
   * @param {*} unfiltered List of unfiltered results for current search engine.
   * @param {*} results List of articles to merge with.
   * @param {*} articlesByLink Index of links to check for duplicates when merging.
   */
  async merge(unfiltered, results, articlesByLink) {
    const current = await unfiltered.$$(this.articleWrapper, this.filterMap)

    for(const index in current) {
      const entry = current[index]
      const article = { 
        title: await entry.$eval(this.filterMap['title'], node => node.innerText), 
        link:  await entry.$eval(this.filterMap['link'], node => node.getAttribute('href')),
        details: await entry.$eval(this.filterMap['details'], node => node.innerText)
      }

      if(article.title != '' && !articlesByLink[article.link]) {
        results.push(article)
        articlesByLink[article.link] = article
      }
    }
  }

  /**
   * Capture an image of the current page for debugging.
   * @param {string} action A string used for identifying the current action in the screenshot.
   * @param {string} name Name of search engine to use in file name.
   * @param {Page} fullPage Puppeteer page to capture screenshot of.
   * @param {bool} fullPage If captured image should be full size. 
   * @param {string} customType png or jpeg.
   */
  async _debugImage(action, page, fullPage, customType) {
    const type = customType ? customType : 'png'
    await page.screenshot({
      path: `./debug_image/${action}-${this.name}.${type}`,
      fullPage: fullPage,
      type: type
    })
  }
}

