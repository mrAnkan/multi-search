# MultiSearch
Search using multiple search engines. The engines currently used are Google and DuckDuckGo.


## Prepare and run for development
`npm i && npm start`

## License
[GPL-3.0-or-later](COPYING)
